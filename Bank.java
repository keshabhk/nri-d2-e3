package assignment3;

public abstract class Bank {

	abstract void getBalance();
}

class BankA extends Bank{
	void getBalance()
	{
	  System.out.println("Balance in BankA: $100");
	}
}

class BankB extends Bank{
	void getBalance()
	{
		System.out.println("Balance in BankA: $150");
	}
}

class BankC extends Bank{
	void getBalance()
	{
		System.out.println("Balance in BankA: $200");
	}
}

