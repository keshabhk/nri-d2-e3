package assignment3;

import java.util.*;

//custom Exception
class CityNotFoundException extends Exception{
	public CityNotFoundException(String message)
	{
		super(message);
	}
}
class City{  
	
	public static Map<Integer,String> map=new HashMap<Integer,String>();
	
		  //city with their pin code is stored in map.
      
	static {
	  map.put(102345,"DELHI");    
      map.put(102456,"KOLKATA");    
      map.put(102567,"LUCKNOW");
      map.put(102577,"SHIMLA");
      map.put(102597,"GANGTOK");
	}
	
	  public String findCityByZipCode(int zipCode)
	{
	   //run a loop by its zip code.
	try {
	  for (Integer zip : map.keySet()) 
            if(zip == zipCode)
            	return map.get(zip);
	  throw new CityNotFoundException("City not found");
	  //if this part runs, it means no city is found with gievn zip code, so throw an error.
	  
	}
	catch(Exception e)
	{
		e.printStackTrace();
		return "";
	}
	}
	  	
	
	public static void main(String args[])
	{
		City c = new City();
		System.out.println(map);
		Scanner sc = new Scanner(System.in);
		System.out.println("\nEnter zipcode: ");
		int inp = sc.nextInt();
		System.out.println(c.findCityByZipCode(inp));
	}
	        
}  