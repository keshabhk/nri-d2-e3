package assignment3;
import java.util.Scanner;
public class CalcTester {
	public static void main(String args[])
	{
		MyCalculator obj = new MyCalculator();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter n: ");
		int n = sc.nextInt();
		int val = obj.divisor_sum(n);
		if (val==0)
			System.out.println("Value exceeds the limit, please enter value less than 1001.");
		else
			System.out.println("The sum of all divisors of "+n+" is: "+val);
		sc.close();
	}
}
